# EasyWiki 易维基 - 简洁、高效的开放内容系统


## 【核心特性】

 1. **词条正文** 支持 **MarkDown 代码**
 2. **词条分类** 采用 MediaWiki **标签式词条**模式
 3. **正文模板** 直接使用 [EasyWebApp 引擎扩展的 HTML 标签规则](http://git.oschina.net/Tech_Query/EasyWebApp#三-数据填充)
 4. 外观天生 **响应式设计**、Google **Material Design** 风格
 5. **Web 前端程序** 可独立部署、使用， **Git/SVN 友好**
 6. 基于 [**jQuery 兼容 API**](http://git.oschina.net/Tech_Query/iQuery) 构建，方便二次开发
 7. 内置 **数据迁移工具**（爬虫式）


## 【版本简史】

**开发进展** —— http://git.oschina.net/Tech_Query/EasyWiki/milestones

 - v0.9 Beta —— 2016年3月31日
   - 新增 **用户权限组**机制（预置 读者、作者、编者、管理员 四大角色权限）
 - v0.8 Beta —— 2016年3月25日
   - 新增 **用户系统**
   - 新增 **Web 词条编辑器**
 - v0.6 Beta —— 2016年3月15日
   - 支持 **词条名搜索**
   - 内置 **爬虫式数据迁移工具**
 - v0.4 Beta —— 2016年3月4日
   - 支持 **`#!` SEO 规则**（基于 PHP）
   - 发布 **Docker 部署**（[灵雀云仓库](https://hub.alauda.cn/repos/techquery/easywiki)）
 - v0.3 Beta —— 2016年2月26日  [Web 前端可独立部署](http://git.oschina.net/Tech_Query/EasyWiki/milestones/1)


## 【特别感谢】

没有之前多个项目的宝贵经验，就没有本项目的诞生 ——

 1. http://git.oschina.net/Tech_Query/MDwiki_zh
 2. http://git.oschina.net/Tech_Query/WikiWand_China
 3. http://git.oschina.net/Tech_Query/EasyWebApp

没有这些优秀的开源软件，就没有本项目 **迅速而稳健的开发** ——

 1. https://github.com/SegmentFault/HyperDown
 2. http://phpquery-library.blogspot.com/
 3. http://git.oschina.net/Tech_Query/EasyLibs.php
 4. http://git.oschina.net/pandao/editor.md